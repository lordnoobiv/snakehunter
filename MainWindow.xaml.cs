﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.IO;
using System.Diagnostics;
using MahApps.Metro.Controls;
using System.Windows.Interop;


namespace PythonFindExec
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {

        
        Button[] bList = new Button[32];
        string[] fullPaths = new string[32];

        public MainWindow()
        {
            InitializeComponent();
            //baseButton.Visibility = Visibility.Hidden;
            filepathBox.Text = Properties.Settings.Default.chosenPath;
            if (filepathBox.Text != "enter directory here...")
            {
                filepathBox.Foreground = Brushes.White;
            }

      

            var bitmapSource = Imaging.CreateBitmapSourceFromHBitmap(Properties.Resources.programicon.GetHbitmap(),
                                  IntPtr.Zero,
                                  Int32Rect.Empty,
                                  BitmapSizeOptions.FromEmptyOptions());

            this.Icon = bitmapSource;

            HiddenFileCheckbox.IsChecked = Properties.Settings.Default.IgnoreHiddenFiles;

            

        }

        private void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (filepathBox.Text == "enter directory here...")
            {
                filepathBox.Text = "";
                filepathBox.Foreground = Brushes.White;
            }
        }


        private void buttonSearchNew_Click(object sender, RoutedEventArgs e)
        {

        }

        private void newSearch_press(object sender, RoutedEventArgs e)
        {

            

            List<string> fileList = search();

            for (int i = 0; i < bList.Length; i++)
            {
                try
                {
                    Button button = bList[i];
                    
                    
                    Console.Write("\nbList[" + i + "] = " + button.Name);
                    if (i <= fileList.Count - 1)
                    {
                        string fileName = fileList.ElementAt(i);
                        fullPaths[i] = fileName; // Store full filenames for...later
                        button.Visibility = Visibility.Visible;
                        button.Content = Path.GetFileName(fileName);
                    }
                    else
                    {
                        button.Visibility = Visibility.Hidden;
                    }
                }
                catch
                {
                    Console.Write("\ni = " + i);
                    // I don't give a shit
                }
            }
        }



        private static List<string> search()
        {
            int skipped = 0;
            List<string> fileList = new List<string>();
            string path = ((MainWindow)Application.Current.MainWindow).filepathBox.Text;

            if (File.Exists(path)) {
                UpdateStatusLabel("The given directory is a file.");
            }

            else if(Directory.Exists(path))
            {

            
                try
                {
                    DirectoryInfo dInfo = new DirectoryInfo(path);
                    if (dInfo.Attributes.HasFlag(FileAttributes.Hidden) & Properties.Settings.Default.IgnoreHiddenFiles)
                    {
                        throw new Exception("The given directory was hidden and was immediately ignored.");
                    }

                    foreach (string fileIterated in Directory.GetFiles(path, "*.py", SearchOption.AllDirectories))
                    {

                        FileAttributes attributes = File.GetAttributes(fileIterated);
                        if (attributes.HasFlag(FileAttributes.Hidden) & Properties.Settings.Default.IgnoreHiddenFiles)
                        {
                            skipped++;
                            continue; // u cant c me
                        }
                        fileList.Add(fileIterated);
                    }

                    int resultCount = fileList.Count;

                    if (resultCount <= 0)
                    {
                        UpdateStatusLabel("No Python files were found in the search area.");
                    }

                    else
                    {
                        UpdateStatusLabel(resultCount.ToString() + " Python files were found in the search area.");
                    }
  
                }
                catch (System.Exception exception)
                {

                    UpdateStatusLabel("An error occurred:\n" + exception.Message);
                }
            }
            else
            {
                UpdateStatusLabel("The specified path was neither a directory nor filepath.");
            }

            return fileList;
        }
        private static void UpdateStatusLabel(string newText)
        {
            ((MainWindow)Application.Current.MainWindow).statusLabel.Content = newText;
        }

        private void updatePathSetting(object sender, TextChangedEventArgs e)
        {
            if (filepathBox.Text != "enter directory here...")
            {
                Properties.Settings.Default.chosenPath = filepathBox.Text;
                Properties.Settings.Default.Save();
                Properties.Settings.Default.Reload();
               
            }
        }

        private int deriveNumber(string name)
        {
            string stringInt = name.Replace("baseButton_Copy", "");

            int index;

            if (stringInt == "baseButton") { index = 0; }
            else if (stringInt == "") { index = 1; }
            else { index = int.Parse(stringInt) + 1; }
            

            
            return index;
        }

        private void initializeButtonUnit(object sender, EventArgs e)
        {

            Button target = (Button)sender;
            target.Visibility = Visibility.Hidden;
            bList[deriveNumber(target.Name)] = target;


        }

        private void executeCMD(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            int no = deriveNumber(button.Name);
            string fullPath = fullPaths[no];

            string command = " python \"" + fullPath + "\"";
            command = "/k" + command + " \nPAUSE";

            Console.Write(command);

            // create the ProcessStartInfo using "cmd" as the program to be run,
            // and "/c " as the parameters.
            // Incidentally, /c tells cmd that we want it to execute the command that follows,
            // and then exit.
            System.Diagnostics.ProcessStartInfo procStartInfo =
            new System.Diagnostics.ProcessStartInfo("cmd", command);

            // The following commands are needed to redirect the standard output.
            // This means that it will be redirected to the Process.StandardOutput StreamReader.

            procStartInfo.UseShellExecute = true;

            // Now we create a process, assign its ProcessStartInfo and start it
            System.Diagnostics.Process proc = new System.Diagnostics.Process();
            proc.StartInfo = procStartInfo;

            proc.Start();
            proc.WaitForExit();

        }

        private void commandLoadSetting(object sender, EventArgs e)
        {
            commandTextBox.Text = Properties.Settings.Default.PythonCommand;
        }

        private void commandTypingNew(object sender, TextChangedEventArgs e)
        {
            if (commandTextBox.Text != @"C:\Python33\python.exe")
            {
                Properties.Settings.Default.PythonCommand = commandTextBox.Text;
                Properties.Settings.Default.Save();
                Properties.Settings.Default.Reload();
            }
        }

        private void HiddenFileCheckedChange(object sender, RoutedEventArgs e)
        {
            CheckBox culprit = (CheckBox)sender;
            Properties.Settings.Default.IgnoreHiddenFiles = (bool)culprit.IsChecked;
            Properties.Settings.Default.Save();
            Properties.Settings.Default.Reload();

        }



   
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new Ookii.Dialogs.Wpf.VistaFolderBrowserDialog();
            if (dialog.ShowDialog(this).GetValueOrDefault())
            {
                filepathBox.Text = dialog.SelectedPath;
                filepathBox.Foreground = Brushes.White;
            }
        }

    }
}
